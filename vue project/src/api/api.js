import axios from 'axios';

const token = '1017|O6pjHt5KFbyeISUCjo1mvaIHS6k9N4kv1ZJ4EYkk'
const instance = axios.create({
    baseURL : 'https://statistr.com',
    timeout : 3000
    
});
export default {
  //  GET method
  get(url,param ={}) {
    return instance.get(url,param)
      .then(response => response.data)
      .catch(error => {
        throw new Error(error);
      });
  },
  //  POST method
  post(url, data) {
    return instance.post(url, data)
      .then(response => response.data)
      .catch(error => {
        throw new Error(error);
      });
  },
  //  PUT method
  put(url, data) {
    return instance.put(url, data)
      .then(response => response.data)
      .catch(error => {
        throw new Error(error);
      });
  },
  //  DELETE method 
  delete(url) {
    return instance.delete(url)
      .then(response => response.data)
      .catch(error => {
        throw new Error(error);
      });
  }
};